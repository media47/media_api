package main

import (
	"media_pb/api"
	"media_pb/api/handlers"
	"media_pb/config"

	"media_pb/services"
	"media_pb/pkg/logger"
	"github.com/gin-gonic/gin"
	)

func main() {
	cfg := config.Load()

	grpcSvcs, err := services.NewGrpcClients(cfg)
	if err != nil {
		panic(err)
	}
	var loggerLevel = new(string)
	*loggerLevel = logger.LevelDebug
	
	switch cfg.Environment {
	case config.DebugMode:
		*loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		*loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		*loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger("media_api", *loggerLevel)
	defer func() {
		err := logger.Cleanup(log)
		if err != nil {
			return
		}
		}()
		
		r := gin.New()

		r.Use(gin.Logger(), gin.Recovery())
		
		h := handlers.NewHandler(cfg, log, grpcSvcs)
		
		api.SetUpAPI(r, h, cfg)
		
		if err := r.Run(cfg.HTTPPort); err != nil {
			return
		}
	}
	