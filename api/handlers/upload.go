package handlers

import (
	"context"
	"fmt"
	"mime/multipart"
	"os"
	"strings"

	https "media_pb/api/http"

	"github.com/minio/minio-go/v7/pkg/credentials"

	"media_pb/pkg/logger"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/minio/minio-go/v7"
)

type UploadResponse struct {
	Filename string `json:"filename"`
}

var (
	defaultBucket = "images"
)

type File struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

type Path struct {
	Filename string `json:"filename"`
	Hash     string `json:"hash"`
}

// Upload godoc
// @ID upload_image
// @Security ApiKeyAuth
// @Router /v1/upload [POST]
// @Summary Upload
// @Description Upload
// @Tags file
// @Accept multipart/form-data
// @Produce json
// @Param file formData file true "file"
// @Success 200 {object} http.Response{data=Path} "Path"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Upload(c *gin.Context) {
	var (
		file File
	)
	err := c.ShouldBind(&file)
	if err != nil {
		h.handleResponse(c, https.BadRequest, err.Error())
		return
	}
	fName, _ := uuid.NewRandom()
	file.File.Filename = strings.ReplaceAll(file.File.Filename, " ", "")
	file.File.Filename = fmt.Sprintf("%s_%s", fName.String(), file.File.Filename)
	dst, _ := os.Getwd()

	minioClient, err := minio.New(h.cfg.MinioEndpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(h.cfg.MinioAccessKeyID, h.cfg.MinioSecretAccessKey, ""),
		Secure: h.cfg.MinioSSL,
	})
	h.log.Info("info", logger.String("access_key: ",
		h.cfg.MinioAccessKeyID), logger.String("access_secret: ", h.cfg.MinioSecretAccessKey))

	if err != nil {
		h.handleResponse(c, https.BadRequest, err.Error())
		return
	}

	err = c.SaveUploadedFile(file.File, dst+"/"+file.File.Filename)
	if err != nil {
		h.handleResponse(c, https.BadRequest, err.Error())
		return
	}
	splitedContentType := strings.Split(file.File.Header["Content-Type"][0], "/")
	if splitedContentType[0] != "image" && splitedContentType[0] != "video" {
		defaultBucket = "docs"
	}

	_, err = minioClient.FPutObject(
		context.Background(),
		defaultBucket,
		file.File.Filename,
		dst+"/"+file.File.Filename,
		minio.PutObjectOptions{ContentType: file.File.Header["Content-Type"][0]},
	)
	if err != nil {
		h.handleResponse(c, https.BadRequest, err.Error())
		os.Remove(dst + "/" + file.File.Filename)

		return
	}

	os.Remove(dst + "/" + file.File.Filename)

	h.handleResponse(c, https.Created, Path{
		Filename: file.File.Filename,
		Hash:     fName.String(),
	})
}
