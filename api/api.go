package api

import (
	"media_pb/api/docs"
	"media_pb/api/handlers"
	"media_pb/config"

	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)


func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {

	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	// docs.SwaggerInfo.Host = cfg.ServiceHost + cfg.HTTPPort
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	r.GET("/ping", h.Ping)
	r.GET("/config", h.GetConfig)

	v1 := r.Group("/v1")
	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization
	// v1.Use(h.AuthMiddleware())
	{
		v1.POST("/upload", h.Upload)

		//Contact
		v1.POST("/contact", h.CreateContact)
		v1.GET("/contact/:contact_id", h.GetSingleContact)
		v1.GET("/contact", h.GetContactList)
		v1.PUT("/contact", h.UpdateContact)
		v1.DELETE("/contact/:contact_id", h.DeleteContact)

		//Vacancy
		v1.POST("/vacancy", h.CreateVacancy)
		v1.GET("/vacancy/:vacancy_id", h.GetSingleVacancy)
		v1.GET("/vacancy", h.GetVacancyList)
		v1.PUT("/vacancy", h.UpdateVacancy)
		v1.DELETE("/vacancy/:vacancy_id", h.DeleteVacancy)

		//JobApp
		// v1.POST("/job_app", h.CreateJobApp)
		// v1.GET("/job_app/:job_app_id", h.GetSingleJobApp)
		// v1.GET("/job_app", h.GetJobAppList)
		// v1.PUT("/job_app", h.UpdateJobApp)
		// v1.DELETE("/job_app/:job_app_id", h.DeleteJobApp)

	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

}



func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}
