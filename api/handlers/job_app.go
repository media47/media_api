package handlers

import (
	"context"
	"media_pb/api/http"
	cs "media_pb/genproto/content_service"
	"media_pb/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateJobApp godoc
// @Security ApiKeyAuth
// @ID create_job_app
// @Router /v1/job_app [POST]
// @Summary Create JobApp
// @Description Create JobApp
// @Tags JobApp
// @Accept json
// @Produce json
// @Param JobApp body content_service.CreateJobAppRequest true "CreateJobAppRequestBody"
// @Success 201 {object} http.Response{data=content_service.JobApp} "JobApp data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateJobApp(c *gin.Context) {
	var jobapp cs.CreateJobAppRequest

	err := c.ShouldBindJSON(&jobapp)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.JobAppService().Create(
		context.Background(),
		&jobapp,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSingleJobApp godoc
// @Security ApiKeyAuthContact
// @ID get_job_app_by_id
// @Router /v1/job_app/{job_app_id} [GET]
// @Summary Get single jobApp
// @Description Get single jobApp
// @Tags JobApp
// @Accept json
// @Produce json
// @Param job_app_id path string true "job_app_id"
// @Success 200 {object} http.Response{data=content_service.JobApp} "JobAppBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSingleJobApp(c *gin.Context) {
	jobappID := c.Param("job_app_id")

	if !util.IsValidUUID(jobappID) {
		h.handleResponse(c, http.InvalidArgument, "JobApp id is an invalid uuid")
		return
	}
	resp, err := h.services.JobAppService().GetSingle(
		context.Background(),
		&cs.JobAppPrimaryKey{
			Id: jobappID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}


// UpdateJobApp godoc
// @Security ApiKeyAuth
// @ID update_job_app
// @Router /v1/job_app [PUT]
// @Summary Update JobApp
// @Description Update JobApp
// @Tags JobApp
// @Accept json
// @Produce json
// @Param JobApp body content_service.JobApp true "UpdateJobAppRequestBody"
// @Success 200 {object} http.Response{data=content_service.JobApp} "JobApp data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateJobApp(c *gin.Context) {
	var jobapp cs.JobApp

	err := c.ShouldBindJSON(&jobapp)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	resp, err := h.services.JobAppService().Update(
		context.Background(),
		&jobapp,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}



// DeleteJobApp godoc
// @Security ApiKeyAuth
// @ID	delete_job_app
// @Router /v1/job_app/{job_app_id} [DELETE]
// @Summary Delete JobApp
// @Description Delete JobApp
// @Tags JobApp
// @Accept json
// @Produce json
// @Param job_app_id path string true "job_app_id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteJobApp(c *gin.Context) {
	jobappID := c.Param("job_app_id")

	if !util.IsValidUUID(jobappID) {
		h.handleResponse(c, http.InvalidArgument, "JobApp id is an invalid uuid")
		return
	}

	resp, err := h.services.JobAppService().Delete(
		context.Background(),
		&cs.JobAppPrimaryKey{
			Id: jobappID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}


// GetJobAppList godoc
// @Security ApiKeyAuth
// @ID get_job_app_list
// @Router /v1/job_app [GET]
// @Summary Get JobApp list
// @Description Get JobApp list
// @Tags JobApp
// @Accept json
// @Produce json
// @Param filters query content_service.GetJobAppsListRequest true "filters"
// @Success 200 {object} http.Response{data=content_service.GetJobAppsListResponce} "JobAppBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetJobAppList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.JobAppService().GetList(
		context.Background(),
		&cs.GetJobAppsListRequest{
			Limit: int32(limit),
			Offset: int32(offset),
			Studies: c.DefaultQuery("studies", ""),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}