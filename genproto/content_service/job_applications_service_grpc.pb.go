// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.17.3
// source: job_applications_service.proto

package content_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// JobAppServiceClient is the client API for JobAppService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type JobAppServiceClient interface {
	Create(ctx context.Context, in *CreateJobAppRequest, opts ...grpc.CallOption) (*JobApp, error)
	GetSingle(ctx context.Context, in *JobAppPrimaryKey, opts ...grpc.CallOption) (*JobApp, error)
	GetList(ctx context.Context, in *GetJobAppsListRequest, opts ...grpc.CallOption) (*GetJobAppsListResponce, error)
	Update(ctx context.Context, in *JobApp, opts ...grpc.CallOption) (*emptypb.Empty, error)
	Delete(ctx context.Context, in *JobAppPrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type jobAppServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewJobAppServiceClient(cc grpc.ClientConnInterface) JobAppServiceClient {
	return &jobAppServiceClient{cc}
}

func (c *jobAppServiceClient) Create(ctx context.Context, in *CreateJobAppRequest, opts ...grpc.CallOption) (*JobApp, error) {
	out := new(JobApp)
	err := c.cc.Invoke(ctx, "/content_service.JobAppService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobAppServiceClient) GetSingle(ctx context.Context, in *JobAppPrimaryKey, opts ...grpc.CallOption) (*JobApp, error) {
	out := new(JobApp)
	err := c.cc.Invoke(ctx, "/content_service.JobAppService/GetSingle", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobAppServiceClient) GetList(ctx context.Context, in *GetJobAppsListRequest, opts ...grpc.CallOption) (*GetJobAppsListResponce, error) {
	out := new(GetJobAppsListResponce)
	err := c.cc.Invoke(ctx, "/content_service.JobAppService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobAppServiceClient) Update(ctx context.Context, in *JobApp, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/content_service.JobAppService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobAppServiceClient) Delete(ctx context.Context, in *JobAppPrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/content_service.JobAppService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// JobAppServiceServer is the server API for JobAppService service.
// All implementations must embed UnimplementedJobAppServiceServer
// for forward compatibility
type JobAppServiceServer interface {
	Create(context.Context, *CreateJobAppRequest) (*JobApp, error)
	GetSingle(context.Context, *JobAppPrimaryKey) (*JobApp, error)
	GetList(context.Context, *GetJobAppsListRequest) (*GetJobAppsListResponce, error)
	Update(context.Context, *JobApp) (*emptypb.Empty, error)
	Delete(context.Context, *JobAppPrimaryKey) (*emptypb.Empty, error)
	mustEmbedUnimplementedJobAppServiceServer()
}

// UnimplementedJobAppServiceServer must be embedded to have forward compatible implementations.
type UnimplementedJobAppServiceServer struct {
}

func (UnimplementedJobAppServiceServer) Create(context.Context, *CreateJobAppRequest) (*JobApp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedJobAppServiceServer) GetSingle(context.Context, *JobAppPrimaryKey) (*JobApp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSingle not implemented")
}
func (UnimplementedJobAppServiceServer) GetList(context.Context, *GetJobAppsListRequest) (*GetJobAppsListResponce, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedJobAppServiceServer) Update(context.Context, *JobApp) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedJobAppServiceServer) Delete(context.Context, *JobAppPrimaryKey) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedJobAppServiceServer) mustEmbedUnimplementedJobAppServiceServer() {}

// UnsafeJobAppServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to JobAppServiceServer will
// result in compilation errors.
type UnsafeJobAppServiceServer interface {
	mustEmbedUnimplementedJobAppServiceServer()
}

func RegisterJobAppServiceServer(s grpc.ServiceRegistrar, srv JobAppServiceServer) {
	s.RegisterService(&JobAppService_ServiceDesc, srv)
}

func _JobAppService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateJobAppRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobAppServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/content_service.JobAppService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobAppServiceServer).Create(ctx, req.(*CreateJobAppRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobAppService_GetSingle_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(JobAppPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobAppServiceServer).GetSingle(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/content_service.JobAppService/GetSingle",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobAppServiceServer).GetSingle(ctx, req.(*JobAppPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobAppService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetJobAppsListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobAppServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/content_service.JobAppService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobAppServiceServer).GetList(ctx, req.(*GetJobAppsListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobAppService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(JobApp)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobAppServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/content_service.JobAppService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobAppServiceServer).Update(ctx, req.(*JobApp))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobAppService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(JobAppPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobAppServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/content_service.JobAppService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobAppServiceServer).Delete(ctx, req.(*JobAppPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// JobAppService_ServiceDesc is the grpc.ServiceDesc for JobAppService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var JobAppService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "content_service.JobAppService",
	HandlerType: (*JobAppServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _JobAppService_Create_Handler,
		},
		{
			MethodName: "GetSingle",
			Handler:    _JobAppService_GetSingle_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _JobAppService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _JobAppService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _JobAppService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "job_applications_service.proto",
}
