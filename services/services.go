package services

import (
	"media_pb/config"
	"media_pb/genproto/content_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	ContactService() content_service.ContactServiceClient
	VacancyService() content_service.VacancyServiceClient
	JobAppService() content_service.JobAppServiceClient
}

type grpcClients struct {
	contactService 	content_service.ContactServiceClient
	vacancyService	content_service.VacancyServiceClient
	jobappService	content_service.JobAppServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	connMediaService, err := grpc.Dial(
		cfg.MediaServiceHost + cfg.MediaGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		contactService: content_service.NewContactServiceClient(connMediaService),
		vacancyService: content_service.NewVacancyServiceClient(connMediaService),
		jobappService: 	content_service.NewJobAppServiceClient(connMediaService),
	},nil
}

func (g *grpcClients) ContactService() content_service.ContactServiceClient {
	return g.contactService
}

func (g *grpcClients) VacancyService() content_service.VacancyServiceClient {
	return g.vacancyService
}

func (g *grpcClients) JobAppService() content_service.JobAppServiceClient {
	return g.jobappService
}